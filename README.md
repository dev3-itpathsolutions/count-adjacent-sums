# README #

# Problem #
---------------
Write the function countAdjacentSums(arr, n) which takes an array and number.It should count the number of times that two adjacent numbers in an array add upto n.
Use Array#forEach.

Examples:

countAdjacentSums([1, 5, 1], 6) => 2

countAdjacentSums([7, 2, 4, 6], 7) => 0

countAdjacentSums([6, 7, 11, 2, 5, 10, 3], 13) => 3


# Solution #
---------------

Step 1: Loop on each element of array

Step 2: Count two adjacent element sum and compare to n

Step 3: Store increment count where adjacent sum is equals to n

Step 4: Print total count

