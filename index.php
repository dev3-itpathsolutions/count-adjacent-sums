<?php

$array = array(6, 7, 11, 2, 5, 10, 3);
$n = 13;
echo countAdjacenSums($array, $n);
function countAdjacenSums($array,$n){
	$tem_array = array();
	$count = 0;
	if(!empty($array )){
		foreach ($array as $key => $value) {
			if($key+1 < count($array)){
				$adjacentSum = $value + $array[$key+1];
				if($adjacentSum == $n){
					$count++;	
				}
			}
		}
	}
	return  $count;
}
